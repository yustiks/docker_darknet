## Различные сборки Docker-контейнеров для Data Scientist'ов ОЦРВ

### TODO причесать, добавить больше зависимостей в Dockerfile


### Ручная сборка:
1. build:
```
docker build --tag <user>_image docker/jupyter_cuda_10_0/
```
2. clear old contaner:
```
docker stop <user>_jupyter || echo
docker rm <user>_jupyter || echo
```
3. start: 
```
docker run -d --name <user>_jupyter -v /home/<user>/:/home/ -p <port>:8888 <user>_image
```

### Example:
```
docker build --tag shtekhin_image docker/jupyter_cuda_10_0/
docker stop shtekhin_jupyter || echo
docker rm shtekhin_jupyter || echo
docker run -d --name shtekhin_jupyter -v /home/shtekhin/:/home/ -p 8889:8888 shtekhin_image
```

docker run --name shtekhin_jupyter -v /home/shtekhin/:/home/ shtekhin_image

# WARNING! Docker-compose не работает, если много контейнеров, юзайте ручную сборку
#### Как юзать через docker-compose:
1. git clone <...>
2. cd jupyter
3. убедиться, что создан юзер на сервере, а также есть доступ к /home/<user>
4. отредактировать файл docker-compose.yml по не заполненным полям <>
5. добавить в docker/jupyter_cuda_10_0/requirements.txt дополнительные зависимости
6. отредактировать поле "--NotebookApp.password=u'sha1:8443902749b1:09202bf125345a4e5620c07e0ab6bb2a7eea67a7'" в docker/jupyter_cuda_10_0/Dockerfile
7. сборка: `sudo docker-compose build`
8. старт: `sudo docker-compose up -d`
9. стоп: `sudo docker-compose down`
